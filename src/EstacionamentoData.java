
public interface EstacionamentoData {

	int getVagasDisponiveis();

	void salvarCarro(CarroEstacionado carroEstacionado) throws EstacionamentoLotadoException;

	CarroEstacionado getCarroEstacionado(CarroEstacionado carroEstacionado) throws CarroNaoExisteException;

	void apagarCarro(CarroEstacionado carroEstacionado) throws CarroNaoExisteException;

}