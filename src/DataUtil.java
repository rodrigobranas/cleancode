import java.util.Date;

public class DataUtil {
	
	public static boolean isDentroDoPeriodo(Date inicio, Date fim, Date hora) {
		return hora.after(inicio) && hora.before(fim);
	}

	public static long calcularPeriodoEmHoras(Date inicio, Date fim) {
		return ((fim.getTime() - inicio.getTime())/(1000*60*60));
	}
}
