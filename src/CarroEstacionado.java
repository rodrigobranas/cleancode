import java.util.Date;

public class CarroEstacionado {
	
	private String placa;
	private Date entrada;

	public CarroEstacionado(String placa) throws PlacaInvalidaException {
		if (isPlacaInvalida(placa)) throw new PlacaInvalidaException();
		this.placa = placa;
	}

	private boolean isPlacaInvalida(String placa) {
		return !placa.matches("[A-Z]{3}-[0-9]{4}");
	}
	
	public String getPlaca() {
		return placa;
	}

	public Date getEntrada() {
		return entrada;
	}

	public void setEntrada(Date entrada) {
		this.entrada = entrada;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((placa == null) ? 0 : placa.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarroEstacionado other = (CarroEstacionado) obj;
		if (placa == null) {
			if (other.placa != null)
				return false;
		} else if (!placa.equals(other.placa))
			return false;
		return true;
	}
}
