import java.util.Date;

public class Periodo {
	private Date inicio;
	private Date fim;
	
	public Periodo(Date inicio, Date fim) throws PeriodoInvalidoException {
		if (fim.before(inicio)) throw new PeriodoInvalidoException();
		this.inicio = inicio;
		this.fim = fim;
	}

	public Date getInicio() {
		return inicio;
	}

	public Date getFim() {
		return fim;
	}

	public boolean isDentroDoPeriodo(Date hora) {
		return hora.after(inicio) && hora.before(fim);
	}

	public long calcularPeriodoEmHoras() {
		return ((fim.getTime() - inicio.getTime())/(1000*60*60));
	}
}
