
public class Ticket {
	private long tempoDePermanencia;
	private long preco;
	
	public Ticket(long tempoDePermanencia, long preco) {
		super();
		this.tempoDePermanencia = tempoDePermanencia;
		this.preco = preco;
	}

	public long getTempoDePermanencia() {
		return tempoDePermanencia;
	}

	public long getPreco() {
		return preco;
	}
	
	
}
