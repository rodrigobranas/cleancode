
public class EstacionamentoDataInMemory implements EstacionamentoData {
	private CarroEstacionado[] carrosEstacionados;
	
	public EstacionamentoDataInMemory(int totalDeVagas) {
		this.carrosEstacionados = new CarroEstacionado[totalDeVagas];
	}
	
	public int getVagasDisponiveis() {
		int vagasDisponiveis = 0;
		for (CarroEstacionado carroEstacionado : carrosEstacionados) {
			if (carroEstacionado == null) vagasDisponiveis++;
		}
		return vagasDisponiveis;
	}

	public void salvarCarro(CarroEstacionado carroEstacionado) throws EstacionamentoLotadoException {
		carrosEstacionados[getVagaDisponivel()] = carroEstacionado;
	}
	
	private int getVagaDisponivel() throws EstacionamentoLotadoException {
		for(int vaga = 0; vaga < carrosEstacionados.length; vaga++) {
			if (carrosEstacionados[vaga] == null) {
				return vaga;
			}
		}
		throw new EstacionamentoLotadoException();
	}
	
	public CarroEstacionado getCarroEstacionado(CarroEstacionado carroEstacionado) throws CarroNaoExisteException {
		return carrosEstacionados[getVagaDoCarroEstacionado(carroEstacionado)];
	}
	
	private int getVagaDoCarroEstacionado(CarroEstacionado carroEstacionado) throws CarroNaoExisteException {
		for (int vaga = 0; vaga < carrosEstacionados.length; vaga++) {
			if (carroEstacionado.equals(carrosEstacionados[vaga])) return vaga;
		}
		throw new CarroNaoExisteException();
	}

	public void apagarCarro(CarroEstacionado carroEstacionado) throws CarroNaoExisteException {
		carrosEstacionados[getVagaDoCarroEstacionado(carroEstacionado)] = null;
	}
}
