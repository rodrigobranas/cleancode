import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

public class EstacionamentoServiceTest {
	private static final int TOTAL_DE_VAGAS = 500;
	private static final String PLACA = "MMX-9999";
	private EstacionamentoService estacionamento;

	@Before
	public void before() throws PeriodoInvalidoException {
		EstacionamentoData estacionamentoData = new EstacionamentoDataInMemory(TOTAL_DE_VAGAS);
		Periodo periodoDeFuncionamento = new Periodo(as(8), as(22));
		TabelaDePreco tabelaDePreco = new TabelaDePrecoSemanal();
		estacionamento = new EstacionamentoService(estacionamentoData, tabelaDePreco, periodoDeFuncionamento);		
	}
	
	@Test
	public void deveTer500VagasDisponiveis() {
		int vagas = estacionamento.getVagasDisponiveis();
		assertEquals(500, vagas);
	}
	
	@Test
	public void deveEntrarUmCarro() throws Exception {
		estacionamento.salvarCarro(new CarroEstacionado(PLACA), as(9));
		int vagas = estacionamento.getVagasDisponiveis();
		assertEquals(499, vagas);
	}
	
	@Test
	public void deveSairUmCarro() throws Exception {
		estacionamento.salvarCarro(new CarroEstacionado(PLACA), as(9));
		estacionamento.apagarCarro(new CarroEstacionado(PLACA), as(20));
		int vagas = estacionamento.getVagasDisponiveis();
		assertEquals(500, vagas);
	}
	
	@Test(expected = CarroDuplicadoException.class)
	public void deveLancarUmaExceptionSeOCarroEstiverDuplicado() throws Exception {
		estacionamento.salvarCarro(new CarroEstacionado(PLACA), as(9));
		estacionamento.salvarCarro(new CarroEstacionado(PLACA), as(9));
	}
	
	@Test(expected = EstacionamentoLotadoException.class)
	public void deveLancarUmaExceptionSeOEstacionamentoEstiverLotado() throws Exception {
		EstacionamentoData estacionamentoData = new EstacionamentoDataInMemory(0);
		Periodo periodoDeFuncionamento = new Periodo(as(8), as(22));
		TabelaDePreco tabelaDePreco = new TabelaDePrecoSemanal();
		estacionamento = new EstacionamentoService(estacionamentoData, tabelaDePreco, periodoDeFuncionamento);
		estacionamento.salvarCarro(new CarroEstacionado(PLACA), as(9));
	}
	
	@Test(expected = CarroNaoExisteException.class)
	public void deveLancarUmaExceptionSeOCarroNaoExistir() throws Exception {
		estacionamento.apagarCarro(new CarroEstacionado(PLACA), as(20));
	}
	
	@Test(expected = PlacaInvalidaException.class)
	public void deveLancarUmaExceptionSeAPlacaForInvalida() throws Exception {
		estacionamento.salvarCarro(new CarroEstacionado("AAA-888"), as(9));
	}
	
	@Test(expected = ForaDoHorarioException.class)
	public void deveLancarUmaExceptionSeTentarEntrarForaDoHorario() throws Exception {
		estacionamento.salvarCarro(new CarroEstacionado("AAA-8888"), as(7));
	}
	
	@Test(expected = ForaDoHorarioException.class)
	public void deveLancarUmaExceptionSeTentarSairForaDoHorario() throws Exception {
		estacionamento.apagarCarro(new CarroEstacionado("AAA-8888"), as(23));
	}
	
	@Test(expected = PeriodoInvalidoException.class)
	public void deveLancarUmaExceptionSeOCarroTentarSairAntesDeTerEntrado() throws Exception {
		estacionamento.salvarCarro(new CarroEstacionado("AAA-8888"), as(20));
		estacionamento.apagarCarro(new CarroEstacionado("AAA-8888"), as(19));
	}
	
	@Test
	public void deveCalcularOTempoDePermanencia() throws Exception {
		estacionamento.salvarCarro(new CarroEstacionado("AAA-8888"), as(10));
		Ticket ticket = estacionamento.apagarCarro(new CarroEstacionado("AAA-8888"), as(16));
		assertEquals(ticket.getTempoDePermanencia(), 6);
	}
	
	@Test
	public void deveCalcularOPrecoDuranteASemana() throws Exception {
		estacionamento.salvarCarro(new CarroEstacionado("AAA-8888"), as(10));
		Ticket ticket = estacionamento.apagarCarro(new CarroEstacionado("AAA-8888"), as(16));
		assertEquals(ticket.getPreco(), 60);
	}
	
	@Test
	public void deveCalcularOPrecoNoFimDeSemana() throws Exception {
		EstacionamentoData estacionamentoData = new EstacionamentoDataInMemory(TOTAL_DE_VAGAS);
		Periodo periodoDeFuncionamento = new Periodo(as(8), as(22));
		TabelaDePreco tabelaDePreco = new TabelaDePrecoFimDeSemana();
		estacionamento = new EstacionamentoService(estacionamentoData, tabelaDePreco, periodoDeFuncionamento);
		estacionamento.salvarCarro(new CarroEstacionado("AAA-8888"), as(10));
		Ticket ticket = estacionamento.apagarCarro(new CarroEstacionado("AAA-8888"), as(16));
		assertEquals(ticket.getPreco(), 30);
	}
	
	@Test
	public void deveCalcularOPrecoA() {
		TabelaDePreco tabela = new TabelaDePrecoSemanal();
		assertEquals(10, tabela.calcularPreco(1));
	}
	
	@Test
	public void deveCalcularOPrecoB() {
		TabelaDePreco tabela = new TabelaDePrecoSemanal();
		assertEquals(20, tabela.calcularPreco(2));
	}
	
	private Date as(int hora) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, hora);
		return calendar.getTime();
	}
}











