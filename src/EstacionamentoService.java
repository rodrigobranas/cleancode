import java.util.Date;

public class EstacionamentoService {
	private Periodo periodoDeFuncionamento;
	private EstacionamentoData estacionamentoData;
	private TabelaDePreco tabelaDePreco;
	
	public EstacionamentoService(EstacionamentoData estacionamentoData, TabelaDePreco tabelaDePreco, Periodo periodoDeFuncionamento) throws PeriodoInvalidoException {
		this.periodoDeFuncionamento = periodoDeFuncionamento;
		this.estacionamentoData = estacionamentoData;
		this.tabelaDePreco = tabelaDePreco;
	}
	
	public int getVagasDisponiveis() {
		return this.estacionamentoData.getVagasDisponiveis();
	}

	public void salvarCarro(CarroEstacionado carroEstacionado, Date entrada) throws CarroDuplicadoException, EstacionamentoLotadoException, ForaDoHorarioException {
		if (!periodoDeFuncionamento.isDentroDoPeriodo(entrada)) throw new ForaDoHorarioException();
		if (isCarroEstacionado(carroEstacionado)) throw new CarroDuplicadoException();
		carroEstacionado.setEntrada(entrada);
		this.estacionamentoData.salvarCarro(carroEstacionado);
	}

	private boolean isCarroEstacionado(CarroEstacionado carroEstacionado) {
		try {
			this.estacionamentoData.getCarroEstacionado(carroEstacionado);
			return true;
		} catch (CarroNaoExisteException e) {
			return false;
		}
	}

	public Ticket apagarCarro(CarroEstacionado carroEstacionado, Date saida) throws CarroNaoExisteException, ForaDoHorarioException, PeriodoInvalidoException {
		if (!periodoDeFuncionamento.isDentroDoPeriodo(saida)) throw new ForaDoHorarioException();
		carroEstacionado = this.estacionamentoData.getCarroEstacionado(carroEstacionado);
		Periodo periodoDePermanencia = new Periodo(carroEstacionado.getEntrada(), saida);
		this.estacionamentoData.apagarCarro(carroEstacionado);
		long tempoDePermanencia = periodoDePermanencia.calcularPeriodoEmHoras();
		return new Ticket(tempoDePermanencia, tabelaDePreco.calcularPreco(tempoDePermanencia));
	}
}
